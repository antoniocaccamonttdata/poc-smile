package me.antoniocaccamo.poc.ugs.repo;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import me.antoniocaccamo.poc.ugs.model.OtpEntity;

@Repository
public interface OtpRepository extends JpaRepository<OtpEntity, BigInteger>{

}
