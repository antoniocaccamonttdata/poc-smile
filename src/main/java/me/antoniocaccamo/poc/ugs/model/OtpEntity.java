package me.antoniocaccamo.poc.ugs.model;

import java.math.BigInteger;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author antonio
 *
 */
@Entity
@Table(name = "IL_T_OTP")
@Data @NoArgsConstructor @AllArgsConstructor
public class OtpEntity {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	private BigInteger id;

}
