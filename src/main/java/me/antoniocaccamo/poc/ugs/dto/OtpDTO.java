/**
 * 
 * @author antonio.caccamo on 2021-05-24
 */
package me.antoniocaccamo.poc.ugs.dto;

import java.math.BigInteger;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import me.antoniocaccamo.poc.ugs.model.OtpEntity;

/**
 * @author antonio
 *
 */
@Builder @Data @NoArgsConstructor @AllArgsConstructor
@ApiModel
public class OtpDTO {
	
	public OtpDTO(OtpEntity entity) {
		id = entity.getId();
	}

	private BigInteger id;
}
