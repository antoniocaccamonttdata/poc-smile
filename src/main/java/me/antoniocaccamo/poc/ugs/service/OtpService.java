package me.antoniocaccamo.poc.ugs.service;

import org.springframework.stereotype.Service;

import me.antoniocaccamo.poc.ugs.dto.OtpDTO;
import me.antoniocaccamo.poc.ugs.model.OtpEntity;
import me.antoniocaccamo.poc.ugs.repo.OtpRepository;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;


@Service
public class OtpService {
	
	private final Scheduler scheduler;
	private final OtpRepository otpRepository;
	
	public OtpService(Scheduler scheduler, OtpRepository otpRepository) {
		this.scheduler = scheduler;
		this.otpRepository = otpRepository;
	}

	public Flux<OtpDTO> getAllOtps() {
		Iterable<OtpEntity> iterable = otpRepository.findAll();
		return Flux.fromIterable(iterable)
				.publishOn(scheduler)
				.switchIfEmpty(Flux.empty())
				.map(e -> OtpDTO.builder().id(e.getId()).build())
		;
	}
	
	

}
