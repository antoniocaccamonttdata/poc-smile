package me.antoniocaccamo.poc.ugs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class UgsApplication {

	public static void main(String[] args) {
		SpringApplication.run(UgsApplication.class, args);
	}

}
