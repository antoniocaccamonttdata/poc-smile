package me.antoniocaccamo.poc.ugs.web.rest;


import java.util.Iterator;

import com.google.api.gax.paging.Page;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BucketInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.google.cloud.storage.Storage.BlobListOption;

import org.reactivestreams.Publisher;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/api/storage")
@Slf4j
@Api
public class StorageResource {

    @GetMapping("/{bucket}")
    public Flux<String> retrieve( @PathVariable String bucket){
        log.info("retrieve list of objects for bucket : {}", bucket);

        Storage storage = StorageOptions.newBuilder()
            .setHost("localhost")
            .setProjectId("myproject")
            .build()
            .getService()
        ;

        Page<Blob> blobs = storage.list(bucket, BlobListOption.currentDirectory() );
        Iterator<Blob> iblob = blobs.iterateAll().iterator();
       
        return Flux.empty();
    }
    
}
