package me.antoniocaccamo.poc.ugs.web.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.antoniocaccamo.poc.ugs.dto.OtpDTO;
import me.antoniocaccamo.poc.ugs.service.OtpService;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/api/otps")
@Api("REST API for composite product information.")
public class OtpResource {
	
	private final OtpService otpService;
	
	public OtpResource(OtpService otpService) {
		this.otpService = otpService;
	}
	
	@GetMapping
	@ApiOperation(
		value = "retrieve otp",
		response = OtpDTO.class)
	public Flux<OtpDTO> getAllOtps() {
		
		return otpService.getAllOtps();
	}
}
