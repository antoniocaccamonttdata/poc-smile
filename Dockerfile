FROM adoptopenjdk/openjdk11:jre-11.0.9.1_1-alpine

RUN addgroup --system javauser && adduser -S -s /bin/false -G javauser javauser
COPY target/*.jar app.jar
USER javauser
EXPOSE 8080
CMD java -XX:+UnlockExperimentalVMOptions -Dcom.sun.management.jmxremote -noverify ${JAVA_OPTS} -jar app.jar